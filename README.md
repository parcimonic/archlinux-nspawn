# Set up a systemd-nspawn container

This is a simple Ansible playbook that should be used with `ansible-pull` to configure an Arch Linux nspawn container. The container setup uses `pacstrap` to bootstrap the container files. Check if your OS has this package before proceeding. Moreover, the graphics setup is configured only for AMD drivers.

More information in the [Arch wiki](https://wiki.archlinux.org/title/Systemd-nspawn#Create_and_boot_a_minimal_Arch_Linux_container).

## Project status

I've stopped working on this project because you can't use vulkan with nspawn: https://github.com/systemd/systemd/issues/10960

The playbook can still be used to configure a host with a basic Steam install, or you can copy the Ansible tasks and reuse on your own playbooks.

## Initial setup

We want the nspawn container to be discoverable by `machinectl`. Therefore, we create the root filesystem at `/var/lib/machines/<machine-name>`, [Arch wiki](https://wiki.archlinux.org/title/Systemd-nspawn#Management) explanation.

Tip: on the host, you can use `sudo journalctl -M games` to check the machine logs and `sudo machinectl <stop/restart> games` to stop/restart the machine.

The container use case is to isolate programs related to gaming, so we'll use `games` as the machine name for the examples below.

1. Create a directory for the root filesystem:

    `# mkdir /var/lib/machines/games`

2. Bootstrap the base filesystem and the basic packages to make Ansible work:

    `# pacstrap -c /var/lib/machines/games base ansible git`

3. Disable private networking for the container. Create `/etc/systemd/nspawn/games.nspawn` on the host, with contents:

    ```text
    [Network]
    VirtualEthernet=no
    ```

4. Chroot into the container and set the root password. This password is temporary and will be disabled by the playbook later.

    ```text
    $ sudo machinectl start games
    $ sudo machinectl shell games
    # passwd
    # logout
    ```

    1. Ensure you can log in as root:

        ```shell
        $ sudo machinectl start games
        $ sudo machinectl login games
        ```

    2. If the login fails, check [this section](https://wiki.archlinux.org/title/Systemd-nspawn#Root_login_fails) of the Arch wiki.

## Applying the playbook

1. Log in as root and apply the playbook:

    ```text
    $ sudo machinectl start games
    $ sudo machinectl login games
    # ansible-pull --checkout trunk --url https://gitlab.com/parcimonic/archlinux-nspawn.git --directory /tmp/ansible --inventory /tmp/ansible/hosts
    ```

    1. Set a password for `archie` (the non-root user): `# passwd archie`

2. On the host, edit `/etc/systemd/nspawn/games.nspawn`, adding the bindings to enable graphics:

    ```text
    [Files]
    Bind=/dev/dri
    BindReadOnly=/tmp/.X11-unix/X1
    ```
    1. Note: if your Xorg server on the host is not using socket X1 (i.e. DISPLAY=:1 for your user), replace the second line with the appropriate value.

    2. Reboot the container: `$ sudo machinectl restart games`

3. On the host, [enable local connections](https://wiki.archlinux.org/title/Chroot#Run_graphical_applications_from_chroot) to the X server: `xhost +local:`

    1. To revert the operation, use: `xhost -local:`

4. Log in as `archie`, export your DISPLAY variable (e.g. `export DISPLAY=:1`), then try to [open Steam](https://wiki.archlinux.org/title/steam#Usage).

## To-do

- Enable sound

- Enable controllers

## References:

- https://discuss.linuxcontainers.org/t/overview-gui-inside-containers/8767

- https://discuss.linuxcontainers.org/t/howto-use-the-hosts-wayland-and-xwayland-servers-inside-containers/8765

- https://discuss.linuxcontainers.org/t/audio-via-pulseaudio-inside-container/8768

- https://wiki.archlinux.org/title/Systemd-nspawn

- https://wiki.archlinux.org/title/Chroot
